def handle_uploaded_file(fname,f):
    """
    Saves file to selected location
    :param fname: File name
    :param f: File
    :return:
    """
    with open(fname, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)