from django.contrib.auth.models import User
from django.db import models
from django.forms import forms


class Location(models.Model):
    name = models.CharField(max_length=200)
    isMonitored = models.BooleanField()
    isAvailable = models.BooleanField()
    user = models.ForeignKey(User)
    uuid = models.CharField(max_length=200)


class UploadFileForm(forms.Form):
    title = models.CharField(max_length=200)
    file = models.FileField()
