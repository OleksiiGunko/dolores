import json
from urllib.parse import parse_qs

from channels import Group
from channels.auth import channel_session_user_from_http, channel_session_user


@channel_session_user_from_http
def ws_connect(message):
    params = parse_qs(message.content['query_string'])
    location_name = params.get(b'location', (None,))[0].decode('utf-8')
    group_name = location_name + message.user.username
    print(group_name)
    message.reply_channel.send({"accept": True})
    message.channel_session['group_name'] = group_name
    Group(group_name).add(message.reply_channel)

@channel_session_user
def ws_message(message):
    msg_text = json.loads(message.content['text'])
    location_name = msg_text['name']
    group_name = location_name + message.user.username
    Group(group_name).send({'text': message.content['text']})


@channel_session_user
def ws_disconnect(message):
    Group(message.channel_session['group_name']).discard(message.reply_channel)