from django.conf.urls import url

from dolores import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^cabinet/', views.cabinet, name='cabinet'),
    url(r'^media_agent/', views.media_agent, name='media_agent'),
    url(r'no_channels/', views.no_channels, name='no_channels'),
    url(r'locations/', views.locations, name='locations'),
    url(r'login/', views.login_view, name='login'),
    url(r'logout/', views.logout_view, name='logout')
]