/*
    mserver.js - this file contains logic for media communication
*/


var localVideo;
var remoteVideo;
var peerConnection;
var uuid;
var isCaller;
var captureStarted = false;

// for server recording
var server_recording;
var mediaRecorder;
var recordedChunks;
var remoteStream;

// images for processing
var currentImage = null;
var oldImage = null;
var sensitivity, temp1Canvas, temp1Context, temp2Canvas, temp2Context, topLeft, bottomRight;


// location name should be changed!!!
var locationName = "";

var peerConnectionConfig = {
    'iceServers': [
        {'urls': 'stun:stun.services.mozilla.com'},
        {'urls': 'stun:stun.l.google.com:19302'},
    ]
};

function pageReady(caller) {
    uuid = getUuid();
    isCaller = caller;

    // registering group
    if(caller == false)
    {
        localVideo = document.getElementById('localVideo');
    }
    else
    {
        locationName = document.getElementById('locationName').value;
        serverConnection = new WebSocket('ws://'+window.location.host+'/media_server/?location=' + locationName);
        serverConnection.onmessage = gotMessageFromServer;

        remoteVideo = document.getElementById('remoteVideo');
    }

    var constraints = {
        video: true,
        audio: true,
    };

    if(navigator.mediaDevices.getUserMedia) {
        navigator.mediaDevices.getUserMedia(constraints).then(getUserMediaSuccess).catch(errorHandler);
    } else {
        alert('Your browser does not support getUserMedia API');
    }
}

// updating location
function update_location(sender) {
    console.log('update location');
    locationName = sender.text;
    pageReady(true);
}

function download_recorded() {
  var blob = new Blob(recordedChunks, {
    type: 'video/webm'
  });
  var url = URL.createObjectURL(blob);
  var a = document.createElement('a');
  document.body.appendChild(a);
  a.style = 'display: none';
  a.href = url;
  a.download = 'recording.webm';
  a.click();
  window.URL.revokeObjectURL(url);
}


function getUserMediaSuccess(stream) {
    localStream = stream;
    if (isCaller === false)
    {
        console.log('getUserMediaSuccess');
        localVideo.src = window.URL.createObjectURL(stream);
    }
}

function start() {

    peerConnection = new RTCPeerConnection(peerConnectionConfig);
    peerConnection.onicecandidate = gotIceCandidate;
    peerConnection.onaddstream = gotRemoteStream;

    // changing adding stream!!
    peerConnection.addStream(localStream);

    if(isCaller) {
        peerConnection.createOffer().then(createdDescription).catch(errorHandler);
    }
    else
    {
        register_on_server();
        server_recording = document.getElementById('cbRecord').checked;
        console.log('starting server recording', server_recording);
        if(server_recording === true)
        {
            recordedChunks = [];

            var options = {mimeType: 'video/webm;codecs=vp9'};
            mediaRecorder = new MediaRecorder(localStream, options);
            mediaRecorder.ondataavailable = handleDataAvailable;
            mediaRecorder.start();
        }
    }

    captureStarted = true;
}

// start recording remote stream
function record_remote() {

    if(remoteStream !== undefined)
    {
        recordedChunks = [];
        var options = {mimeType: 'video/webm;codecs=vp9'};
        mediaRecorder = new MediaRecorder(localStream, options);
        mediaRecorder.ondataavailable = handleDataAvailable;
        mediaRecorder.start();
    }
}

function stop_record() {
    if(mediaRecorder) {
        mediaRecorder.stop();
    }
}

function handleDataAvailable(event) {
              if (event.data.size > 0) {
                recordedChunks.push(event.data);
              } else {
                console.log('size 0')
              }
}

//sending registration information on server
function register_on_server()
{
    locationName = document.getElementById('locationName').value;
    console.log(locationName);

    serverConnection = new WebSocket('ws://'+window.location.host+'/media_server/?location=' + locationName);
        serverConnection.onmessage = gotMessageFromServer;
        serverConnection.onopen = function (data)
        {
            serverConnection.send(JSON.stringify({'method': 'register', 'uuid': uuid, name: locationName }));
        }
        serverConnection.onclose = function (data)
        {
            serverConnection.send(JSON.stringify({'method': 'unregister', 'uuid': uuid, name: locationName}));
        }

    var data = {'uuid':uuid, 'name':locationName };
    var url = 'http://'+window.location.host+'/dolores/locations/';
    $.ajax({
       url: url,
       type: 'POST',
       contentType:'application/json',
       data: JSON.stringify(data),
       dataType:'json'
     });
}

// stops capturing of media
function stop_media()
{
    localStream.getTracks().forEach(track => track.stop())
    captureStarted = false;

    // removing from db
    var data = {'uuid':uuid, 'name': locationName};
    var url = 'http://'+window.location.host+'/dolores/locations/';
    $.ajax({
       url: url,
       type: 'DELETE',
       contentType:'application/json',
       data: JSON.stringify(data),
       dataType:'json'
     });

     if(server_recording)
     {
        mediaRecorder.stop();
     }
}

// playing recoded video
function play_recorded() {
  var superBuffer = new Blob(recordedChunks);
  console.log(superBuffer);
  localVideo.src = window.URL.createObjectURL(superBuffer);
}

function gotMessageFromServer(message) {
    if(!captureStarted) return;
    if(!peerConnection) start(false);

    var signal = JSON.parse(message.data);

    // Ignore messages from ourself
    if(signal.uuid == uuid) return;

    if(signal.sdp) {
        peerConnection.setRemoteDescription(new RTCSessionDescription(signal.sdp)).then(function() {
            // Only create answers in response to offers
            if(signal.sdp.type == 'offer') {
                peerConnection.createAnswer().then(createdDescription).catch(errorHandler);
            }
        }).catch(errorHandler);
    } else if(signal.ice) {
        peerConnection.addIceCandidate(new RTCIceCandidate(signal.ice)).catch(errorHandler);
    }
}

function gotIceCandidate(event) {
    console.log('for ice candidate', event);
    if(event.candidate != null) {
        serverConnection.send(JSON.stringify({'ice': event.candidate, 'uuid': uuid, 'name': locationName}));
    }
}

function createdDescription(description) {
    console.log('got description');

    peerConnection.setLocalDescription(description).then(function() {
        serverConnection.send(JSON.stringify({'sdp': peerConnection.localDescription, 'uuid': uuid, name: locationName}));
    }).catch(errorHandler);
}

function gotRemoteStream(event) {
    console.log('got remote stream');
    if(isCaller === true)
    {
        remoteVideo.src = window.URL.createObjectURL(event.stream);
        remoteStream = event.stream;
    }
}

function errorHandler(error) {
    console.log(error);
}

// Taken from http://stackoverflow.com/a/105074/515584
// Strictly speaking, it's not a real UUID, but it gets the job done here
function getUuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}


/*
		 * Captures a still image from the video.
		 *
		 * @param <Element> append An optional element where we want to append the image.
		 *
		 * @return <Element> A canvas element with the image.
		 *
		 */
function captureImage() {
			var canvas = document.createElement('canvas');
			canvas.width = 640;
			canvas.height = 480;
			canvas.getContext('2d').drawImage(localVideo, 0, 0, 640, 480);

			var pngImage = canvas.toDataURL("image/png");
			console.log(canvas);

			return canvas;
}

function compareImages()
{
    oldImage = currentImage;
	currentImage = captureImage();

			if(!oldImage || !currentImage) {
				return;
			}

			var motion_detected = compare(currentImage, oldImage, 640, 480);
			if(motion_detected)
			{
			    console.log('motion detected');
			}
}

/*
		 * Initializes the object.
		 * Also used as a reset between image comparements.
		 *
		 * @return void.
		 *
		 */
		function initialize_canvas() {
			sensitivity = 40;

			if(!temp1Canvas) {
				temp1Canvas = document.createElement('canvas');
				temp1Context = temp1Canvas.getContext("2d");
			}

			if(!temp2Canvas) {
				temp2Canvas = document.createElement('canvas');
				temp2Context = temp2Canvas.getContext("2d");
			}

			topLeft = [Infinity,Infinity];
			bottomRight = [0,0];
		}


/*
		 * Compares to images.
		 *
		 * @param <Element> image1 The canvas of the first image.
		 * @param <Element> image2 The canvas of the second image.
		 * @param <Int>		width  The width to compare.
		 * @param <Int>		height The height to compare
		 *
		 * @return <Object> The top left, and the bottom right pixels.
		 *
		 */
		function compare(image1, image2, width, height) {
			initialize_canvas();

			if(!image1 || !image2) {
				return;
			}

			temp1Context.clearRect(0,0,100000,100000);
			temp1Context.clearRect(0,0,100000,100000);

			temp1Context.drawImage(image1, 0, 0, width, height);
			temp2Context.drawImage(image2, 0, 0, width, height);


			for(var y = 0; y < height; y++) {
				for(var x = 0; x <  width; x++) {
					var pixel1 = temp1Context.getImageData(x,y,1,1);
					var pixel1Data = pixel1.data;

					var pixel2 = temp2Context.getImageData(x,y,1,1);
					var pixel2Data = pixel2.data;

					if(comparePixel(pixel1Data, pixel2Data) == false) {
					    return true;
					}
				}
			}

			return false;
		}


/*
		 * Compares an individual pixel (within a range based on sensitivity).
		 *
		 * @param <Array> p1 The first pixel [r,g,b,a].
		 * @param <Array> p2 The second pixel [r,g,b,a].
		 *
		 * @return <Boolean> If they are the same.
		 *
		 */
		function comparePixel(p1, p2) {
			var matches = true;

			for(var i = 0; i < p1.length; i++) {
				var t1 = Math.round(p1[i]/10)*10;
				var t2 = Math.round(p2[i]/10)*10;

				if(t1 != t2) {
					if((t1+sensitivity < t2 || t1-sensitivity > t2)) {
						matches = false;
					}
				}
			}

			return matches;
}