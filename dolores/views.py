import json
import logging

from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from dolores.models import Location, UploadFileForm
from dolores.storage_logic import handle_uploaded_file

MAX_USER_CHANNELS = 5
logger = logging.getLogger(__name__)


def index(request):
    logger.debug('index() - called')
    return render(request, 'dolores/index.html')

@login_required
def cabinet(request):
    try:
        logger.debug('cabinet() - called by %s', request.user)
        locations = Location.objects.filter(user=request.user)
        if len(locations) == 0:
            return render(request, 'dolores/no_clients.html')
    except Exception:
        logger.error("Can't find locations for user %s", request.user)
        raise Http404("Can't find locations")
    return render(request, 'dolores/cabinet.html', {'locations':locations, 'location': locations[0] })

@login_required()
@csrf_exempt
def locations(request):
    """
    Recieves location information in post request
    and saving it to db
    :param request:
    :return:
    """
    try:
        if request.method == 'POST':
            json_data = json.loads(request.body.decode('utf8'))
            new_location = Location()
            new_location.user = request.user
            new_location.name = json_data['name']
            new_location.uuid = json_data['uuid']
            new_location.isMonitored = False
            new_location.isAvailable = True
            # saving to db
            new_location.save()
            logger.debug('locations() - saving location %s for user %s', new_location.name, request.user.username)
            return HttpResponse(status=201)

        if request.method == 'DELETE':
            json_data = json.loads(request.body.decode('utf8'))
            location = Location.objects.get(name=json_data['name'])
            location.delete()
            logger.debug('locations() - removing location %s for user %s', json_data['name'])
            return HttpResponse(status=200)

    except Exception as exc:
        logger.error("locations() - exception occured",exc)
        return HttpResponse(status=500)


def login_view(request):
    if request.method == 'GET':
        return render(request, 'dolores/login.html')
    if request.method == 'POST':
        username = request.POST.get('inputEmail')
        password = request.POST.get('inputPassword')
        print(password)
        user = authenticate(username=username, password=password)
        print(user)
        if user is not None:
            if user.is_active:
                login(request, user)
                return render(request, 'dolores/index.html')
            else:
                return render(request, 'dolores/login.html', {'errors:': 'User account is disabled'})
        else:
            return render(request, 'dolores/login.html', {'errors': 'Login is failed' })


@login_required
def logout_view(request):
    logout(request)
    return render(request, 'dolores/index.html')

@login_required
def media_agent(request):
    locations = Location.objects.filter(user=request.user)
    if len(locations) >= MAX_USER_CHANNELS:
        logger.info('media_agent() - user %s out of channels', request.user.username)
        return render(request, 'dolores/no_channels.html')
    return render(request, 'dolores/media_agent.html')

def no_channels(request):
    return render(request, 'dolores/no_channels.html')

def upload_file(request):
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            handle_uploaded_file(form.title, request.FILES['file'])
            return HttpResponse(status=201)
    return HttpResponse(status=500)