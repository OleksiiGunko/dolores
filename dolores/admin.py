from django.contrib import admin

# Register your models here.
from dolores.models import Location

admin.site.register(Location)
