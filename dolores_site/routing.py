from channels.routing import route

channel_routing = {
    'websocket.connect': 'dolores.consumers.ws_connect',
    'websocket.receive': 'dolores.consumers.ws_message',
    'websocket.disconnect': 'dolores.consumers.ws_disconnect',
}

